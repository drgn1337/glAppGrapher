## Introduction

![Sample 1](docs/img/sample1.jpg) ![Sample 2](docs/img/sample2.jpg) ![Sample 3](docs/img/sample3.jpg)

The glAppGrapher is an interactive graphical server that allows clients to indirectly render three-dimensional objects. It is intended to be a debugging tool for software developers who need to visualize aspects of their programs without turning their programs upside-down in the process. This server, in conjunction with a properly written client side, has the potential to:

* Assist a programmer in debugging a physics engine or collision detection code through the visual output of certain objects and key vectors
* Allow the programmer to visualize BSP and other complex trees both topologically and graphically via spheres, planes and meshes, as well as what objects are associated with what nodes.
* Involve more than one computer.
* Do all of this in an interactive environment, even if the client process is halted.

## Features

* Ability to navigate around the scene with the mouse
* A scalable interactive console (it currently does nothing)
* Full-Screen or Windowed support
* Uses Berkeley Socket function calls for communication between the glAppGrapher and clients
* Single glAppGrapher server supports multiple clients.
* The ability to enqueue lists of messages, and execute them all at once

## Building

The src\glAppGrapherServer folder contains a solution that opens in Visual Studio 2015 or better. Though the glAppGrapher code is designed to be cross-platform, it does not come with any makefiles for any other compiling platforms nor has it been tested on them lately.

## Using

Refer to the [server documentation](docs/server.md) for usage.

# Using the glAppGrapher Server

## Command-line Parameters

| Parameter | Type | Description |
|------|---|----|
| -fullscreen  | | If this parameter is present, the server will run in full screen mode. The default mode is 800x600x16@60 Hz |
| -width  | int | Initial width of the window, or width in full screen mode  |
| -height  | int | Initial height of the window, or height in full screen mode |
| -bpp  | int | Initial bits per pixel in full screen mode (Default is 16) |
| -refreshrate  | int | Refresh rate in full screen mode (Default is 60 Hz) |
| -port  | int | Listen port (default is 17702) |

## How to use it

The glAppGrapher contains a list of OpenGL and GLU-based commands. Each time you send the glAppGrapher a command to either render something or change a render property, the glAppGrapher will add your command to the linked list. Every time the glAppGrapher redraws itself because of a camera movement, a window refresh, or the act of having a command sent to it; it will clear the scene, and traverse the entire linked list for commands to execute.

For example, if you send these commands to the glAppGrapher from your code only once:

`glColor4f(1,0.5,0.5,0);`
`glVertex3f(0,0,0);`

The glAppGrapher will perform this sequence of events every time it redraws:

`glColor4f(1,0.5,0.5,0);`
`glVertex3f(0,0,0);`

So, how does the server know you want it to call "glColor4f" and "glVertex3f"? Because in the command you pass to it, you send the name of the OpenGL function you want to call, as well as all its parameters. If any of those parameters are a pointer, you will just pass all the raw data that is pointed to. Later on when the glAppGrapher actually handles your command, it maps the name of the OpenGL function to the actual OpenGL function itself, passes in the parameters that you gave it, and calls the function.
/***************************************************************************
                          gfxobj.cpp  -  description
                             -------------------
    begin                : Sun Sep  1 17:01:23 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "gfxobj.h"
#include "gfx.h"
#include "msg.h"
#include "../shared/mem.h"

#include "glut.h"  /* Header File For The GLUT Library */
#include <GL/gl.h>	/* Header File For The OpenGL32 Library */
#include <GL/glu.h>	/* Header File For The GLu32 Library */

#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <math.h>

/* Used for rendering spheres */
extern GLUquadricObj* g_pQuadratic;

typedef struct {
	float x,y,z;
} _VECTOR;

inline float DotProduct(const _VECTOR& v1, const _VECTOR& v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

// Cross product
inline _VECTOR operator *(const _VECTOR& v1, const _VECTOR& v2)
{
	_VECTOR vc;
	vc.x = v1.y*v2.z - v1.z*v2.y;
	vc.y = v1.z*v2.x - v1.x*v2.z;
	vc.z = v1.x*v2.y - v1.y*v2.x;
	return vc;
}

inline _VECTOR operator *(const _VECTOR& v, float fScalar)
{
	_VECTOR vc;
	vc.x = v.x * fScalar;
	vc.y = v.y * fScalar;
	vc.z = v.z * fScalar;
	return vc;
}

inline _VECTOR operator -(const _VECTOR& v1, const _VECTOR& v2)
{
	_VECTOR v;
	v.x = v1.x - v2.x;
	v.y = v1.y - v2.y;
	v.z = v1.z - v2.z;
	return v;
}

inline _VECTOR operator /(const _VECTOR& v, float fScalar)
{
	_VECTOR vc;
	vc.x = v.x / fScalar;
	vc.y = v.y / fScalar;
	vc.z = v.z / fScalar;
	return vc;
}

inline void Normalize(_VECTOR& v)
{
	float d = v.x*v.x + v.y*v.y + v.z*v.z;
	d = (float)sqrt(d);
	v = v / d;
}

CGfxObj::CGfxObj(void* pCmdData, int nCmdSize, int iGroup /* = 0 */)
{
	m_pCmdData = mem_allocateandcopy(nCmdSize, pCmdData);
	m_pFunc = 0;
	m_pStack = 0;
	m_nStackSize = 0;
	m_pNext = 0;
  m_iGroup = iGroup;

	/* Given the command, grab a pointer to the function and build
	the stack, if applicable */
	if (MapCommandData())
	{
		if (m_pCmdData)
		{
			mem_free(m_pCmdData);
			m_pCmdData = 0;
		}
	}
}

CGfxObj::~CGfxObj()
{
	if (m_pCmdData) mem_free(m_pCmdData);
	if (m_pStack) mem_free(m_pStack);
}

/* Given our command, grab a pointer to the function and build
the stack */
int CGfxObj::MapCommandData()
{
    if (!m_pCmdData) return -1;
	const char* szFunc = (const char*)m_pCmdData;
	/*int flags = *(int*)(szFunc + strlen(szFunc) + 1);*/
	int nParams = *(int*)(szFunc + strlen(szFunc) + 1 + sizeof(int) /* Flags */);
	char* pParams = (char*)(szFunc + strlen(szFunc) + 1 + sizeof(int) /* Flags */ + sizeof(int) /* Parameter count */);
	int i;

	/* Quit if the function does not exist */
	if (!(m_pFunc=gfx_getfuncpointer(szFunc)))
		return 0;

	/* Build the stack to push later */
	char* p = pParams;
	m_nStackSize = 0; /* Stack size in bytes */
	/* Calculate the size of the stack */
	for (i=0; i < nParams; i++)
	{
		int nParamSize = *(int*)p;
		if (nParamSize & GLAG_IS_POINTER)
			m_nStackSize += sizeof(void*);
		else
			m_nStackSize += nParamSize;

		p = p + sizeof(int) + (nParamSize & ~GLAG_IS_POINTER); /* Skip param size and data fields */
	}
	m_pStack = mem_allocate(m_nStackSize);
	if (!m_pStack)
		return -1;

	/* Fill the stack */
	p = pParams;
	for (i=0; i < nParams; i++)
	{
		int nParamSize = *(int*)p;
		if (nParamSize & GLAG_IS_POINTER)
		{
			memmove((char*)m_pStack+sizeof(void*), m_pStack, m_nStackSize - sizeof(void*));
			*(void**)m_pStack = (void*)(p + sizeof(int));
		}
		else
		{
			memmove((char*)m_pStack+nParamSize, m_pStack, m_nStackSize - nParamSize);
			memcpy(m_pStack, (void*)(p + sizeof(int)), nParamSize);
		}
		p = p + sizeof(int) + (nParamSize & ~GLAG_IS_POINTER); /* Skip param size and data fields */
	}
	return 0;
}

int CGfxObj::CallMappedFunction()
{
	/* This is where things get hard core. We push the stack and call the function. */
	int i = m_nStackSize >> 2;
	int j = (int)m_pStack;
	int k = (int)m_pFunc;
#ifdef WIN32
	__asm {
		mov ecx,i
		mov eax,j
action:
		push dword ptr [eax]
		add eax,4
		loop action
		call k
	}
#endif
#ifdef __GNUC__
  asm ("movl %0, %%ecx" : : "r"(i));
  asm ("movl %0, %%eax" : : "r"(j));
  asm ("action:");
  asm ("pushl (%eax)");
  asm ("addl $4,%eax");
  asm ("loop action");
  asm ("call *%0" : : "r"(k));
#endif
	return 0;

}

int CGfxObj::DoCommand()
{
    if (!m_pCmdData) return -1;
	/* See if this is a mapped function to which we have the
	address of. If so, call it and move on.
	
	TODO: Allow support for function prototypes through a built-in parser
	to make function calls from the console as easy as calling them through C.
	*/
	if (m_pFunc)
		return CallMappedFunction();

	/* No; there is no member function pointer. We're going to do this
	the hard way. */
	const char* szFunc = (const char*)m_pCmdData;
	char* pParams = (char*)(szFunc + strlen(szFunc) + 1 + sizeof(int) /* Flags*/ + sizeof(int) /* Parameter count */);

	if (!strcmp(szFunc, "GLAG_DrawSpheres"))
	{
		int nSpheres = *(int*)(pParams + sizeof(int));
		float* p = (float*)(pParams + sizeof(int) + sizeof(int) + sizeof(int));
		DrawSpheres(nSpheres, p);
	}
	else if (!strcmp(szFunc, "GLAG_DrawPlanes"))
	{
		int nPlanes = *(int*)(pParams + sizeof(int));
		float* p = (float*)(pParams + sizeof(int) + sizeof(int) + sizeof(int));
		DrawPlanes(nPlanes, p);
	}
	else if (!strcmp(szFunc, "GLAG_Print"))
	{
		gfx_print((char*)(pParams + sizeof(int)));
	}
	else return -1;
	return 0;
}

int CGfxObj::DrawPlanes(int nPlanes, float* pf)
{
	float fPlaneVerts[6*3];

	for (int i=0; i < nPlanes; i++, pf += 4)
	{
		_VECTOR vNorm = { pf[0], pf[1], pf[2] };
		_VECTOR vX = { 1,0,0 };
		_VECTOR vY = { 0,1,0 };
		_VECTOR vZ = { 0,0,1 };
		_VECTOR vA, vB;
		_VECTOR vX1, vX2;
		_VECTOR vOffset;
		float dX = DotProduct(vX, vNorm);
		float dY = DotProduct(vY, vNorm);
		float dZ = DotProduct(vZ, vNorm);

		// Project each vertex onto the plane
		vX = (vX - (vNorm * dX));
		vY = (vY - (vNorm * dY));
		vZ = (vZ - (vNorm * dZ));

		// Now center the vectors at the origin
		vA = vY - vX;
		vB = vZ - vX;

		// Now normalize
		Normalize(vA);
		Normalize(vB);

		// Now calculate 2 perpendicular vectors that lie in the plane
		vX1 = vA * vB;
		vX2 = vA * vX1;

		// Now vA and vX2 lie in the same plane and are perpendicular.
		// Scale them.

		vA = vA * 500;
		vX2 = vX2 * 500;

		// Now calculate the offset (d) component
		vOffset.x = pf[0] * -pf[3];
		vOffset.y = pf[1] * -pf[3];
		vOffset.z = pf[2] * -pf[3];

		// This will help avoid artifacts
		if (vOffset.x >= 0) vOffset.x += 0.1f * pf[0];
		else vOffset.x -= 0.1f;
		if (vOffset.y >= 0) vOffset.y += 0.1f * pf[1];
		else vOffset.y -= 0.1f;
		if (vOffset.z >= 0) vOffset.z += 0.1f * pf[2];
		else vOffset.z -= 0.1f;

		// Now draw the plane
		fPlaneVerts[0] = vOffset.x + vA.x + vX2.x; fPlaneVerts[1] = vOffset.y + vA.y + vX2.y; fPlaneVerts[2] = vOffset.z + vA.z + vX2.z;
		fPlaneVerts[3] = vOffset.x + vA.x - vX2.x; fPlaneVerts[4] = vOffset.y + vA.y - vX2.y; fPlaneVerts[5] = vOffset.z + vA.z - vX2.z;
		fPlaneVerts[6] = vOffset.x + -vA.x - vX2.x; fPlaneVerts[7] = vOffset.y + -vA.y - vX2.y; fPlaneVerts[8] = vOffset.z + -vA.z - vX2.z;

		fPlaneVerts[9] = vOffset.x + -vA.x - vX2.x; fPlaneVerts[10] = vOffset.y + -vA.y - vX2.y; fPlaneVerts[11] = vOffset.z + -vA.z - vX2.z;
		fPlaneVerts[12] = vOffset.x + -vA.x + vX2.x; fPlaneVerts[13] = vOffset.y + -vA.y + vX2.y; fPlaneVerts[14] = vOffset.z + -vA.z + vX2.z;
		fPlaneVerts[15] = vOffset.x + vA.x + vX2.x; fPlaneVerts[16] = vOffset.y + vA.y + vX2.y; fPlaneVerts[17] = vOffset.z + vA.z + vX2.z;

		glVertexPointer(3, GL_FLOAT, sizeof(float)*3, fPlaneVerts);
		glDrawArrays(GL_TRIANGLES, 0, 6);

	}

	return 0;	
}

int CGfxObj::DrawSpheres(int nSpheres, float* pf)
{
	for (int i=0; i < nSpheres; i++, pf += 4)
	{
		glTranslatef(pf[0], pf[1], pf[2]);
		gluSphere(g_pQuadratic, pf[3], 20, 20);
		glTranslatef(-pf[0], -pf[1], -pf[2]);
	}
	return 0;
}

/***************************************************************************
                          msgobj.h  -  description
                             -------------------
    begin                : Thu Nov 7 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __MSGOBJ_H__
#define __MSGOBJ_H__

#include "../shared/mem.h"

#ifndef NULL
#define NULL 0x00000000
#endif

class CMsgObj
{
protected:
  void* m_pData;
  int m_nDataBytes;
  int m_iMessage;
  
  CMsgObj* m_pNext;
public:
	CMsgObj(int iMessage, int nMsgBytes = 0, void* pData = NULL);
	virtual ~CMsgObj();

	CMsgObj* GetNext() { return m_pNext; }
	void SetNext(CMsgObj* pNext) { m_pNext = pNext; }
  int Execute();
};

#endif

/***************************************************************************
                          console.h  -  description
                             -------------------
    begin                : Mon Sep  9 22:52:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/* One-time initialization */
int console_init(int argc, char **argv);

/* Handle a text message */
int console_handle_msg(const char* szMsg);

/* Handle a key press */
int console_handle_key(char c);

/* Draw the console */
int console_draw();

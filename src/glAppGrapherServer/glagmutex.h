/***************************************************************************
                          glagmutex.h  -  description

	Universalizes mutex functionality.

                             -------------------
    begin                : Wed Nov  6 22:36:51 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Much of this code was quickly copied from tutorials by Jeff Molofee   *
 *   at http://nehe.gamedev.net                                            *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#define pthread_mutexattr_default 0
#define pthread_mutex_init(m, attr) *(m) = CreateEvent(NULL, 1 /* Manual reset*/, 1 /* Initial state */, NULL)
#define pthread_mutex_lock(m) \
	{ \
		WaitForSingleObject(*(m), INFINITE); \
		ResetEvent(*(m)); \
	}
#define pthread_mutex_unlock(m) SetEvent(*(m))
#define HMUTEX HANDLE
#elif defined(__GNUC__)
#include <pthread.h>
#define HMUTEX pthread_mutex_t
#ifndef pthread_mutexattr_default
#define pthread_mutexattr_default ((pthread_mutexattr_t*)NULL)
#endif
#else
#error Pick a platform!
#endif

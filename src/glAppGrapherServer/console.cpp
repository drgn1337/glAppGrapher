/***************************************************************************
                          console.cpp  -  description
                             -------------------
    begin                : Mon Sep  9 22:52:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************

 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <string.h>
#include <memory.h>
#include <stdlib.h>
#include "gfx.h"
#include "console.h"

#define MAX_CHARS_PER_LINE		128
#define LINE_HEIGHT 0.07f

enum EConsoleMode {
	eModeHidden, /* Completely invisible */
	eModeBottom, /* On the bottom online; three lines above the cursor */
	eModeHalfScreen, /* Takes up the top half of the screen (transparent background) */
	eModeFullScreen /* Takes up the whole screen */
};

EConsoleMode g_eConsoleMode = eModeBottom;

class CConsoleLine {
public:
	char m_szLine[MAX_CHARS_PER_LINE];
	CConsoleLine* m_pNext;
	float m_fClr[3];

public:
	CConsoleLine(const char* szText, CConsoleLine* pNext = NULL)
	{
		if (szText)
			strncpy_s(m_szLine, MAX_CHARS_PER_LINE, szText, MAX_CHARS_PER_LINE);
		else
			memset(m_szLine, 0, MAX_CHARS_PER_LINE);

		m_fClr[0] = m_fClr[1] = m_fClr[2] = 1;
		m_pNext = pNext;
	};
	virtual ~CConsoleLine() {};
	virtual void Draw(float x, float y)
	{
		gfx_moveto(x,y);
		gfx_print(m_szLine);		
	}
};

CConsoleLine* g_pConsoleLines = 0;
char g_szCurString[MAX_CHARS_PER_LINE + 1] = {0};

int console_init(int argc, char **argv)
{
	return 0;
}

int console_handle_msg(const char* szMsg)
{
	/* TODO: PARSER */
	return -1;
}

int console_handle_key(char c)
{
	switch (c)
	{
	case 0x1B: /* Escape */
		exit(0);
		break;

	case 0x08: /* Backspace */
		if (!strlen(g_szCurString))
			return 0;
		g_szCurString[ strlen(g_szCurString) - 1 ] = 0;
		break;

	case 0x0D: /* Enter */
		console_handle_msg(g_szCurString);
		g_pConsoleLines = new CConsoleLine(g_szCurString, g_pConsoleLines);
		memset(g_szCurString, 0, strlen(g_szCurString));
		break;

	case 0x60: /* Toggle console mode */
		/* TODO */
		break;

	default:
		if (strlen(g_szCurString) >= MAX_CHARS_PER_LINE)
			return 0;
		g_szCurString[strlen(g_szCurString)+1] = 0;
		g_szCurString[strlen(g_szCurString)] = c;
		break;
	}
	return 0;
}

int console_draw()
{
	CConsoleLine* pLine = g_pConsoleLines;
	int i;
	float y;

	switch (g_eConsoleMode)
	{
	case eModeHidden:
		return 0;
	case eModeBottom:
		gfx_color(1,1,1);
		gfx_begintext();

		/* Draw the active line of text */
		gfx_moveto(-0.99f, -0.97f);
		gfx_print("%s_", g_szCurString);
		
		/* Draw the last three lines */
		for (i=0, y=-0.97f + LINE_HEIGHT; i < 3 && pLine; i++, y += LINE_HEIGHT, pLine = pLine->m_pNext)
		{
			gfx_moveto(-0.99f, y);
			gfx_print(pLine->m_szLine);
		}
		gfx_endtext();
		break;
  default:
    break;
	}
	return 0;
}

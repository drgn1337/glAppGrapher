/***************************************************************************
                          gfx.h  -  description
                             -------------------
    begin                : Sun Sep  1 00:52:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __GFX_H__
#define __GFX_H__

#include "msg.h"

/* One-time initialization */
int gfx_init(int argc, char **argv);

/* One-time dynamic loading of libraries */
int gfx_loadlibraries();

/* Returns a graphical function given a name */
void* gfx_getfuncpointer(const char* szFuncName);

/* Clears all command list entries in a specified group */
int gfx_cleargroup(int iGroup);

/* Assigns the current group */
int gfx_setcurrentgroup(int iGroup);

/* Enables or disables screen refreshing */
int gfx_enable_screen_refresh(int bEnable);

/* Standard functions */
int gfx_clear(); /* This doesn't clear the viewport; this empties the command list */
int gfx_addcommand(void* pMsg, int nBytes); /* Add a command to the list */
int gfx_moveto(float x, float y);
int gfx_begintext();
int gfx_print(char* str, ...); /* Print out text */
int gfx_endtext();
int gfx_color(float r, float g, float b);

/* Specialized functions */
int gfx_drawgrid();

#endif

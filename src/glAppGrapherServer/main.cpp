/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Sun Sep  1 00:52:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#include "gfx.h"
#include "msg.h"
#include "console.h"

int main(int argc, char *argv[])
{
  if (msg_init(argc, argv)) /* Initialize our message passing system */
    return EXIT_FAILURE;
  if (console_init(argc, argv)) /* Initialize the console */
	return EXIT_FAILURE;
  if (gfx_init(argc, argv))  /* Initialize our graphics system */
    return EXIT_FAILURE;
  return EXIT_SUCCESS;
}



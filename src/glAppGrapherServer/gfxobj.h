/***************************************************************************
                          gfxobj.h  -  description
                             -------------------
    begin                : Sun Sep  1 17:01:23 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __GFXOBJ_H__
#define __GFXOBJ_H__

#include "msg.h" /* Contains _GL_DRAW structure */

class CGfxObj
{
protected:
	void* m_pCmdData;	/* Client-sent command data */
	void* m_pFunc;		/* Pointer to graphics function */
	void* m_pStack;		/* Stack to use with function */
	int m_nStackSize;	/* Size of the stack */
  int m_iGroup;     /* Which group, if any, is this a member of? */
	CGfxObj* m_pNext; /* Pointer to next graphical object in list */

	virtual int MapCommandData();
	virtual int DrawPlanes(int nPlanes, float* pf);
	virtual int DrawSpheres(int nSpheres, float* pf);
public:
	CGfxObj(void* pCmdData, int nCmdSize, int iGroup = 0);

	virtual ~CGfxObj();

	virtual int CallMappedFunction();
	virtual int DoCommand();

	CGfxObj* GetNext() { return m_pNext; }
	void SetNext(CGfxObj* pNext) { m_pNext = pNext; }
  int GetGroup() { return m_iGroup; }
  void SetGroup(int iGroup) { m_iGroup = iGroup; }
};

#endif

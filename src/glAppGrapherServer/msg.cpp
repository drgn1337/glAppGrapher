/***************************************************************************
                           msg.cpp  -  description
                             -------------------

  Handles communication between clients and this server. Also processes
  messages, and calls gfx functions based on what the message is.

    begin                : Sun Sep  1 00:52:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock.h>
#endif
#ifdef __GNUC__
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <memory.h>
#define closesocket(s) shutdown(s, 2)
#endif
#include <memory.h>
#include <stdlib.h>
#include "msg.h"
#include "gfx.h"
#include "console.h"
#include "../shared/mem.h"

#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif
#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

int g_sListen = INVALID_SOCKET;
unsigned short g_wPort = GLAPPGRAPHERMSG_DEFAULT_PORT;
int g_iFrozen = 0;
int g_iIgnore = 0;
CMsgObj* g_pMsgList = NULL, *g_pMsgTail = NULL;

typedef enum { eGettingMsg, eGettingSize, eGettingData } EStatus;

void OnReceivedData(SOCKET s, int& msg, EStatus& status, char* pcData,
					int& nReceivedBytes, int& nExpectedBytes)
{
	char* c = pcData;
	while (c < pcData + nReceivedBytes)
	{
		int nSize = nExpectedBytes;
		if (nSize > nReceivedBytes)
			return;		

		switch (status)
		{
		case eGettingMsg:
			memcpy(&msg, c, sizeof(int));
			nExpectedBytes = sizeof(int);
			status = eGettingSize;
			break;

		case eGettingSize:
			memcpy(&nExpectedBytes, c, sizeof(int));
			if (nExpectedBytes)
			{
				status = eGettingData;
			}
			else
			{
				if (g_iFrozen)
				{
					if (msg != GLAG_MSG_FREEZE)
					{
						msg_enqueue(new CMsgObj(msg, 0, NULL));
					}
				}
				else
				{
					msg_execute(msg, 0, NULL, s);
				}
				nExpectedBytes = sizeof(int);
				status = eGettingMsg;
			}
			break;

		case eGettingData:
			if (g_iFrozen)
			{
				if (msg == GLAG_MSG_FREEZE)
				{
					g_iFrozen = *((int*)c);
				}
				else
				{
					msg_enqueue(new CMsgObj(msg, nSize, c));
				}
	        }
			else
			{
				msg_execute(msg, nSize, c, s);
		    }
			nExpectedBytes = sizeof(int);
			status = eGettingMsg;
			break;
		}

		memmove(c, c + nSize, nReceivedBytes - nSize);
		nReceivedBytes -= nSize;
	}
}

/* Thread used to communicate with a client */
#ifdef WIN32
DWORD WINAPI MessageThread(LPVOID lpParam)
#define THREAD_FAILED -1
#else
#ifdef unix
void* MessageThread(void* lpParam)
#define THREAD_FAILED (void*)-1
#endif
#endif
{
	int s = (int)lpParam;
	char szInBuf[2048];

	int nBytesRead;
	int nDataSize = 1;

	int msg = 0;
	EStatus status = eGettingMsg;
	char* pcData = NULL;
	int nReceivedBytes = 0;
	int nExpectedBytes = sizeof(int);

	if (s == INVALID_SOCKET)
		return THREAD_FAILED;

	pcData = new char[1];

	/* Receive a complete message */
	while ((nBytesRead=recv(s, szInBuf, 2048, 0)) > 0)
	{
		while (nDataSize < nReceivedBytes + nBytesRead)
		{
			char* p = new char[nDataSize * 2];
			if (!p) return 0;
			memcpy(p, pcData, nDataSize);
			delete pcData;
			pcData = p;
			nDataSize = nDataSize * 2;
		}

		memcpy(pcData + nReceivedBytes, szInBuf, nBytesRead);
		nReceivedBytes += nBytesRead;
		OnReceivedData(s, msg, status, pcData, nReceivedBytes, nExpectedBytes);
	}
	closesocket(s);
	delete[] pcData;
	return 0;
}

/* Thread used to listen for client connections */
#ifdef WIN32
DWORD WINAPI ListenThread(LPVOID lpParam)
#else
#ifdef __GNUC__

void* ListenThread(void* lpParam)
#endif
#endif
{
	struct sockaddr_in sin;
	
	g_sListen = socket(AF_INET, SOCK_STREAM, 0);
	if (g_sListen == INVALID_SOCKET)
		return THREAD_FAILED;

	sin.sin_family=PF_INET;
	sin.sin_port=htons(g_wPort);
	sin.sin_addr.s_addr=INADDR_ANY;
	if(bind(g_sListen,(struct sockaddr*)&sin,sizeof(struct sockaddr))==SOCKET_ERROR)
	{
		closesocket(g_sListen);
		return THREAD_FAILED;
	}
	while (listen(g_sListen,SOMAXCONN) != SOCKET_ERROR)
	{
		int s = accept(g_sListen, 0, 0);

    if (s != INVALID_SOCKET)
    {
#ifdef WIN32
  		unsigned long dwID;
  		if (!CreateThread(NULL, 0, MessageThread, (void*)s, 0, &dwID))
  		{
  			closesocket(s);
  		}
#else
#ifdef __GNUC__
      pthread_t id;
      pthread_create(&id, NULL, MessageThread, (void*)s);
#endif
#endif
    }
	}
	closesocket(g_sListen);
	g_sListen = INVALID_SOCKET;
	return 0;
}

int msg_init(int argc, char **argv)
{
	/* Initialize sockets */
#ifdef WIN32
	WSADATA wsad;
    if(WSAStartup(0x0101,&wsad))
    {
		return -1;
	}
#endif

	/* Handle arguments */
	for (int i=1; i < argc; i++)
	{
		if (!strncmp(argv[i], "-port", strlen("-port")))
		{
			g_wPort = atoi(argv[i] + strlen("-port"));
		}
	}

	/* Create our listen thread */
#ifdef WIN32
	unsigned long dwID, dwParam = 0;
	if (!CreateThread(NULL, 0, ListenThread, &dwParam, 0, &dwID))
		return -1;
#else
#ifdef __GNUC__
  pthread_t id;
  pthread_create(&id, NULL, ListenThread, NULL);
#endif
#endif
	return 0;

}

int msg_enqueue(CMsgObj* pObj)
{
  if (NULL == g_pMsgList)
  {
    g_pMsgList = g_pMsgTail = pObj;
  }
  else
  {
    g_pMsgTail->SetNext(pObj);
    g_pMsgTail = pObj;
  }
  return 0;
}

int msg_flush_queue()
{
  CMsgObj* p = g_pMsgList, *pNext;

  /* Disable actual screen redrawing */
  gfx_enable_screen_refresh(0);
  while (p)
  {
    pNext = p->GetNext();
    p->Execute();
    delete p;
    p = pNext;
  }
  g_pMsgList = g_pMsgTail = NULL;

  /* Enable actual screen redrawing */
  gfx_enable_screen_refresh(1);
  return 0;
}

int msg_execute(int msg, int pendingsize, char* szPendingData, int s)
{
  /* Ignore all messages but GLAG_MSG_IGNORE if g_iIgnore is not 0 */
  if (msg != GLAG_MSG_IGNORE && g_iIgnore != 0)
    return 0;
  
  switch (msg)
	{
  case GLAG_MSG_CLEAR:
    gfx_clear();
    break;
	case GLAG_MSG_CONSOLE:
		console_handle_msg((const char*)szPendingData);
		break;
	case GLAG_MSG_FUNC:
		gfx_addcommand((void*)szPendingData, pendingsize);
		break;
	case GLAG_MSG_SHUTDOWN:
		/* TODO: Password protection */
		exit(0);
		break;
  case GLAG_MSG_SETGROUPID:
    gfx_setcurrentgroup(*((int*)szPendingData));
    break;
  case GLAG_MSG_CLEARGROUP:
    gfx_cleargroup(*((int*)szPendingData));
    break;
  case GLAG_MSG_FREEZE:
    g_iFrozen = *((int*)szPendingData);
    break;
  case GLAG_MSG_FLUSH:
    /* Process our frozen message queue */
    msg_flush_queue();
    break;
  case GLAG_MSG_SYNC:
    {
      char szSyncData[1] = { 0 };
      send(s, szSyncData, 1, 0);
    }
    break;
  case GLAG_MSG_IGNORE:
    g_iIgnore = *((int*)szPendingData);
    break;
  default:
    break;
	}
  return 0;
}

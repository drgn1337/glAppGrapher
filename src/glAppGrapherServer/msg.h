/***************************************************************************
                          msg.h  -  description
                             -------------------
    begin                : Sun Sep  1 00:52:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __MSG_H__
#define __MSG_H__

#include "../shared/glappgraphermsg.h"
#include "msgobj.h"

/* One-time initialization */
int msg_init(int argc, char **argv);

/* Perform a command based on a message */
int msg_execute(int msg, int pendingsize, char* szPendingData, int s);

/* Enqueuing function */
int msg_enqueue(CMsgObj* pObj);

/* Flush the queue */
int msg_flush_queue();

#endif

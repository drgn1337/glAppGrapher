/***************************************************************************
                          msgobj.cpp  -  description
                             -------------------
    begin                : Thu Nov 7 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "msg.h"
 
CMsgObj::CMsgObj(int iMessage, int nMsgBytes /*=0*/, void* pData /* NULL*/)
{
	m_pData = mem_allocateandcopy(nMsgBytes, pData);
	m_iMessage = iMessage;
  m_nDataBytes = nMsgBytes;
	m_pNext = NULL;
}

CMsgObj::~CMsgObj()
{
	if (m_pData)
		mem_free(m_pData);
}

int CMsgObj::Execute()
{
  return msg_execute(m_iMessage, m_nDataBytes, (char*)m_pData, 0);
}

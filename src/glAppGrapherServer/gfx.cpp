/***************************************************************************
                          gfx.cpp  -  description

	Covers all non-command-specific GLUT functionality for the server.

                             -------------------
    begin                : Sun Sep  1 00:52:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Much of this code was quickly copied from tutorials by Jeff Molofee   *
 *   at http://nehe.gamedev.net                                            *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
/* Instances to graphical libraries */
HINSTANCE g_hOpenGL, g_hGLU;

#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")
#elif defined(__GNUC__)
#include <dlfcn.h>
#include <stdarg.h>
void *g_hOpenGL, *g_hGLU;
#else
#error Pick a platform!
#endif

#include "glut.h"  /* Header File For The GLUT Library */
#include <GL/gl.h>
#include <GL/glu.h>
#include <memory.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "glagmutex.h"
#include "gfx.h"
#include "gfxobj.h"
#include "console.h"


/* True if we need to refresh the window */
int g_bRefresh = 0;

/* True if we are enabled to draw */
int g_bEnabled = 1;

/* Linked list of graphical objects to render */
CGfxObj* g_pGfxObj = 0;

/* Mutex for accessing g_pGfxObj */
HMUTEX g_Mutex;

/* Camera coords */
float g_fEye[3];
float g_fCenter[3];
float g_fUp[3];

/* Quadratic */
GLUquadricObj* g_pQuadratic;

/* Mouse states */
unsigned char g_bLButtonDown, g_bMButtonDown, g_bRButtonDown;
int g_ClickX, g_ClickY;

/* Temporary world movement variables */
float g_fOldEye[3];
float g_fOldCenter[3];
float g_fOldUp[3];

/* Window size */
int g_Width, g_Height;

/* Current group */
int g_iCurGroup = 0;

void OnResize(int Width, int Height)
{
  if (Height==0)
    Height=1;

  glViewport(0, 0, Width, Height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(45.0f,1.0f,0.1f,1000.0f);
  glMatrixMode(GL_MODELVIEW);

  g_Width = Width;
  g_Height = Height;

  /* Tell OnDraw to draw the next time it's called */
  g_bRefresh = 1;
}

void OnKeyboard(unsigned char key, int x, int y)
{
	console_handle_key(key);
	/* Tell OnDraw to draw the next time it's called */
	g_bRefresh = 1;
}

void OnMouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			g_bLButtonDown = 1;
			g_bRButtonDown = 0;
			g_bMButtonDown = 0;
			g_ClickX = x;
			g_ClickY = y;
		}
		else
			g_bLButtonDown = 0;

		/* Copy the current camera state */
		memcpy(g_fOldEye, g_fEye, sizeof(float)*3);
		memcpy(g_fOldCenter, g_fCenter, sizeof(float)*3);
		memcpy(g_fOldUp, g_fUp, sizeof(float)*3);
	}
	else if (button == GLUT_MIDDLE_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			g_bLButtonDown = 0;
			g_bRButtonDown = 1;
			g_bMButtonDown = 0;
			g_ClickX = x;
			g_ClickY = y;
		}
		else
			g_bRButtonDown = 0;

		/* Copy the current camera state */
		memcpy(g_fOldEye, g_fEye, sizeof(float)*3);
		memcpy(g_fOldCenter, g_fCenter, sizeof(float)*3);
		memcpy(g_fOldUp, g_fUp, sizeof(float)*3);
	}
	else if (button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			g_bLButtonDown = 0;
			g_bRButtonDown = 0;
			g_bMButtonDown = 1;
			g_ClickX = x;
			g_ClickY = y;
		}
		else
			g_bMButtonDown = 0;

		/* Copy the current camera state */
		memcpy(g_fOldEye, g_fEye, sizeof(float)*3);
		memcpy(g_fOldCenter, g_fCenter, sizeof(float)*3);
		memcpy(g_fOldUp, g_fUp, sizeof(float)*3);
	}

	/* Tell OnDraw to draw the next time it's called */
	g_bRefresh = 1;
}

static __inline void BuildRotationMatrix(float* v, float fRad, float* m)
{
	float sinus = (float)sin(fRad);
	float cosinus = (float)cos(fRad);
	float V = 1 - cosinus;
	float a,b,c;

	a = v[0];
	b = v[1];
	c = v[2];

	m[0] = a*a*V + cosinus;
	m[4] = a*b*V - c*sinus;
	m[8] = a*c*V + b*sinus;
	m[12] = 0;

	m[1] = b*a*V + c*sinus;
	m[5] = b*b*V + cosinus;
	m[9] = b*c*V - a*sinus;
	m[13] = 0;

	m[2] = c*a*V - b*sinus;
	m[6] = c*b*V + a*sinus;
	m[10]= c*c*V + cosinus;
	m[14] = 0;

	m[3] = 0;
	m[7] = 0;
	m[11] = 0;
	m[15] = 1.0f;
}

static __inline void TransformVector(float* v1, float* v2, float* matrix)
{
	float* m = matrix;
	float w;

	*v2 = (*v1) * (*m) + (*(v1+1)) * (*(m+4)) + (*(v1+2)) * (*(m+8)) + (*(m+12));
	*(v2+1) = (*v1) * (*(m+1)) + (*(v1+1)) * (*(m+5)) + (*(v1+2)) * (*(m+9)) + (*(m+13));
	*(v2+2) = (*v1) * (*(m+2)) + (*(v1+1)) * (*(m+6)) + (*(v1+2)) * (*(m+10)) + (*(m+14));
	w = (*v1) * (*(m+3)) + (*(v1+1)) * (*(m+7)) + (*(v1+2)) * (*(m+11)) + (*(m+15));

	if (w != 0) {
		*v2 /= w; *(v2+1) /= w; *(v2+2) /= w;
	}
	else {
		*v2 = 0; *(v2+1) = 0; *(v2+2) = 0;
	}
}

void OnMouseMotion(int x, int y)
{
	if (g_bLButtonDown)
	{
		float fRelX = (g_ClickX - x) / (float)g_Width;
		float fRelY = (g_ClickY - y) / (float)g_Height;
		float v[3], vRes[3], vRight[3];
		float vv[3] = { 0,1,0 };
		float m[16];

		v[0] = g_fOldEye[0] - g_fOldCenter[0];
		v[1] = g_fOldEye[1] - g_fOldCenter[1];
		v[2] = g_fOldEye[2] - g_fOldCenter[2];

		BuildRotationMatrix(vv, fRelX, m);
		TransformVector(v, vRes, m);

		vRight[0] = (g_fOldEye[1] - g_fOldCenter[1])*g_fOldUp[2] - (g_fOldEye[2] - g_fOldCenter[2])*g_fOldUp[1];
		vRight[1] = (g_fOldEye[2] - g_fOldCenter[2])*g_fOldUp[0] - (g_fOldEye[0] - g_fOldCenter[0])*g_fOldUp[2];
		vRight[2] = (g_fOldEye[0] - g_fOldCenter[0])*g_fOldUp[1] - (g_fOldEye[1] - g_fOldCenter[1])*g_fOldUp[0];

		float d = vRight[0]*vRight[0] + vRight[1]*vRight[1] + vRight[2]*vRight[2];
		d = -sqrt(d);
		if (d)
		{
			vRight[0] /= d;
			vRight[1] /= d;
			vRight[2] /= d;
		}

		BuildRotationMatrix(vRight, fRelY, m);
		TransformVector(vRes, v, m);

		g_fEye[0] = g_fCenter[0] + v[0];
		g_fEye[1] = g_fCenter[1] + v[1];
		g_fEye[2] = g_fCenter[2] + v[2];
	}
	else if (g_bRButtonDown)
	{
		float fRelY = (g_ClickY - y) / (float)g_Height;
		float f[3];

		f[0] = g_fOldEye[0] - g_fOldCenter[0];
		f[1] = g_fOldEye[1] - g_fOldCenter[1];
		f[2] = g_fOldEye[2] - g_fOldCenter[2];

		float d = f[0]*f[0] + f[1]*f[1] + f[2]*f[2];
		d = -sqrt(d);
		if (d)
		{
			f[0] /= d;
			f[1] /= d;
			f[2] /= d;
		}

		g_fEye[0] = g_fOldEye[0] + (g_fOldCenter[0] - g_fOldEye[0]) * fRelY;
		g_fEye[1] = g_fOldEye[1] + (g_fOldCenter[1] - g_fOldEye[1]) * fRelY;
		g_fEye[2] = g_fOldEye[2] + (g_fOldCenter[2] - g_fOldEye[2]) * fRelY;
	}
	else if (g_bMButtonDown)
	{
		float fRelX = (g_ClickX - x) / (float)g_Width;
		float fRelY = (g_ClickY - y) / (float)g_Height;
		float vRight[3];

		vRight[0] = (g_fOldEye[1] - g_fOldCenter[1])*g_fOldUp[2] - (g_fOldEye[2] - g_fOldCenter[2])*g_fOldUp[1];
		vRight[1] = (g_fOldEye[2] - g_fOldCenter[2])*g_fOldUp[0] - (g_fOldEye[0] - g_fOldCenter[0])*g_fOldUp[2];
		vRight[2] = (g_fOldEye[0] - g_fOldCenter[0])*g_fOldUp[1] - (g_fOldEye[1] - g_fOldCenter[1])*g_fOldUp[0];

		float d = vRight[0]*vRight[0] + vRight[1]*vRight[1] + vRight[2]*vRight[2];
		d = -sqrt(d);
		if (d)
		{
			vRight[0] /= d;
			vRight[1] /= d;
			vRight[2] /= d;
		}

		g_fEye[0] = g_fOldEye[0] + vRight[0] * fRelX * 40 - g_fOldUp[0] * fRelY * 40;
		g_fEye[1] = g_fOldEye[1] + vRight[1] * fRelX * 40 - g_fOldUp[1] * fRelY * 40;
		g_fEye[2] = g_fOldEye[2] + vRight[2] * fRelX * 40 - g_fOldUp[2] * fRelY * 40;
		g_fCenter[0] = g_fOldCenter[0] + vRight[0] * fRelX * 40 - g_fOldUp[0] * fRelY * 40;
		g_fCenter[1] = g_fOldCenter[1] + vRight[1] * fRelX * 40 - g_fOldUp[1] * fRelY * 40;
		g_fCenter[2] = g_fOldCenter[2] + vRight[2] * fRelX * 40 - g_fOldUp[2] * fRelY * 40;
	}

	/* Tell OnDraw to draw the next time it's called */
	g_bRefresh = 1;
}

void OnDraw()
{
  if (!g_bRefresh) return;
  if (!g_bEnabled) return;

  /* CRITICAL SECTION AGAINST gfx_addcommand */
  pthread_mutex_lock(&g_Mutex);
  g_bRefresh = 0;

  CGfxObj* pObj = g_pGfxObj;
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  gluLookAt(g_fEye[0], g_fEye[1], g_fEye[2], g_fCenter[0], g_fCenter[1], g_fCenter[2],
	  g_fUp[0], g_fUp[1], g_fUp[2]);

  /* Draw the grid */
  gfx_drawgrid();

  /* Draw the console */
  console_draw();

  /* Draw all the objects in the world */
  while (pObj)
  {
	  pObj->DoCommand();
	  pObj = pObj->GetNext();
  }

  glutSwapBuffers();

  /* CRITICAL SECTION AGAINST gfx_addcommand */
  pthread_mutex_unlock(&g_Mutex);
}

void OnForcedDraw()
{
	/* Tell OnDraw to draw the next time it's called */
	g_bRefresh = 1;
	OnDraw();
}

int gfx_init(int argc, char **argv)
{
	unsigned char bFullScreen = 0;
	int bpp = 16;
	int refreshrate = 60;

	/* Load our graphical libraries dynamically so we can map
	commands to functions */
	gfx_loadlibraries();

	/* Create our mutex */
	pthread_mutex_init(&g_Mutex, pthread_mutexattr_default);

	/* Define initial variable values */
	g_fEye[0] = 0; g_fEye[1] = 50; g_fEye[2] = 200;
	g_fCenter[0] = 0; g_fCenter[1] = 0; g_fCenter[2] = 0;
	g_fUp[0] = 0; g_fUp[1] = 1; g_fUp[2] = 0;
	g_Width = 400;
	g_Height = 400;

	/* Handle arguments */
	for (int i=1; i < argc; i++)
	{
		if (!strcmp(argv[i], "-fullscreen"))
		{
			bFullScreen = 1;
			g_Width = 800;

			g_Height = 600;
		}
		else if (!strncmp(argv[i], "-width", strlen("-width")))
		{
			g_Width = atoi(argv[i] + strlen("-width"));
		}
		else if (!strncmp(argv[i], "-height", strlen("-height")))
		{
			g_Height = atoi(argv[i] + strlen("-height"));
		}
		else if (!strncmp(argv[i], "-bpp", strlen("-bpp")))
		{
			bpp = atoi(argv[i] + strlen("-bpp"));
		}
		else if (!strncmp(argv[i], "-refreshrate", strlen("-refreshrate")))
		{
			refreshrate = atoi(argv[i] + strlen("-refreshrate"));
		}
	}

	/* Graphics initialization */ 
	glutInit(&argc, argv);  
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	if (bFullScreen)
	{
		char szGameModeString[128];
		sprintf_s(szGameModeString, 128, "%dx%d:%d@%d", g_Width, g_Height, bpp, refreshrate);
		glutGameModeString(szGameModeString);
		glutEnterGameMode();
	}
	else
	{
		glutInitWindowSize(g_Width, g_Height);
		glutInitWindowPosition(0, 0);  
		glutCreateWindow("glAppGrapher");
	}
	glutDisplayFunc(&OnForcedDraw);  
	glutIdleFunc(&OnDraw);
	glutReshapeFunc(&OnResize);
	glutMouseFunc(&OnMouseButton);
	glutKeyboardFunc(&OnKeyboard);
	glutMotionFunc(&OnMouseMotion);

	/* Scene initialization */ 
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,1.0f,0.1f,1000.0f);
	glMatrixMode(GL_MODELVIEW);
	glEnableClientState (GL_VERTEX_ARRAY);
	g_pQuadratic = gluNewQuadric();

	g_bLButtonDown = 0;
	g_bRButtonDown = 0;
	
	glutMainLoop();  

	/* Return 0 to mean no errors */  
	return 0;
}

/* This doesn't clear the viewport; this empties the command list */
int gfx_clear()
{
  pthread_mutex_lock(&g_Mutex);
	while (g_pGfxObj)
	{
		CGfxObj* pObj = g_pGfxObj->GetNext();
		delete g_pGfxObj;
		g_pGfxObj = pObj;
	}
	g_pGfxObj = 0;

	/* Tell OnDraw to draw the next time it's called */
	g_bRefresh = 1;
  pthread_mutex_unlock(&g_Mutex);  
	return 0;
}

/* Add a command to the list */
int gfx_addcommand(void* pMsg, int nBytes)

{
	/* CRITICAL SECTION AGAINST OnDraw */
	pthread_mutex_lock(&g_Mutex);

	/* TODO: Critical section against OnDraw */
	CGfxObj* pObj = new CGfxObj(pMsg, nBytes, g_iCurGroup);
	CGfxObj* pObjLast = g_pGfxObj;

	if (!pObj) {
		pthread_mutex_unlock(&g_Mutex);
		return -1;
	}

	/* See if this is an immediate command. If so, do it now. */
	const char* szFunc = (const char*)pMsg;
	int flags = *(int*)(szFunc + strlen(szFunc) + 1);
	if (flags & GLAG_CMD_IMMEDIATE)
	{
		pObj->DoCommand();
		delete pObj;
		pthread_mutex_unlock(&g_Mutex);
		return 0;
	}

	if (!pObjLast)
	{
		g_pGfxObj = pObj;
		/* Tell OnDraw to draw the next time it's called */
		g_bRefresh = 1;
		pthread_mutex_unlock(&g_Mutex);
		return 0;
	}
	else while (pObjLast->GetNext())
	{
		pObjLast = pObjLast->GetNext();
	}
	pObjLast->SetNext(pObj);

	/* Tell OnDraw to draw the next time it's called */
	g_bRefresh = 1;

	/* CRITICAL SECTION AGAINST OnDraw */
	pthread_mutex_unlock(&g_Mutex);
	return 0;
}

/* Built in function; lots of room for improvement */
int gfx_drawgrid()
{
	glBegin(GL_LINES);
	glColor3f(0.2f,0.2f,0.2f);
	for (GLfloat z=-200.0f; z <= 200.0f; z+=20.0f)
	{
		if (z >= -0.1f && z <= 0.1f) glColor3f(0.5f,0.5f,0.5f);
		glVertex3f(-200, 0, z);
		glVertex3f(200, 0, z);
		if (z >= -0.1f && z <= 0.1f) glColor3f(0.2f,0.2f,0.2f);
	}
	for (GLfloat x=-200.0f; x <= 200.0f; x+=20.0f)
	{
		if (x >= -0.1f && x <= 0.1f) glColor3f(0.5f,0.5f,0.5f);
		glVertex3f(x, 0, -200);
		glVertex3f(x, 0, 200);
		if (x >= -0.1f && x <= 0.1f) glColor3f(0.2f,0.2f,0.2f);
	}
	glEnd();
	return 0;
}

int gfx_loadlibraries()
{
#ifdef WIN32
	g_hOpenGL = LoadLibrary("opengl32.dll");
	g_hGLU = LoadLibrary("glu32.dll");
	return 0;
#endif
#ifdef __GNUC__
	g_hOpenGL = dlopen("libGL.so", RTLD_LAZY);
	g_hGLU = dlopen("libGLU.so", RTLD_LAZY);
#endif
	return -1;
}

void* gfx_getfuncpointer(const char* szFuncName)
{
#ifdef WIN32
	FARPROC proc;

	/* Check the OpenGL library */
	if (proc=GetProcAddress(g_hOpenGL, szFuncName))
		return (void*)proc;

	/* Check the GLU library */
	if (proc=GetProcAddress(g_hGLU, szFuncName))
		return (void*)proc;
#endif
#ifdef __GNUC__
	void* proc;

	/* Check out the OpenGL library */
	if ((proc=dlsym(g_hOpenGL, szFuncName)))
		return proc;

	/* Check the GLU library */
	if ((proc=dlsym(g_hGLU, szFuncName)))
		return proc;
#endif
	return 0;
}

int gfx_begintext()
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	return 0;
}

int gfx_endtext()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	return 0;
}

int gfx_moveto(float x, float y)
{
	glRasterPos2f(x,y);
	return 0;
}

int gfx_print(char* str, ...)
{
	char sz[4096];
	if (!str) return -1;

	va_list list;
	va_start(list,str);
	vsprintf_s(sz, 4096, str, list);
	va_end(list);

	char* p = sz;
	while (*p != '\0') glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p++);
	return 0;
}

int gfx_color(float r, float g, float b)
{
	glColor3f(r,g,b);
	return 0;
}

int gfx_cleargroup(int iGroup)
{
  pthread_mutex_lock(&g_Mutex);
  
	CGfxObj* pObj = g_pGfxObj, *pNext, *pPrev = NULL;
  if (iGroup == 0)
    return -1;
  
	while (pObj)
	{
    pNext = pObj->GetNext();
    if (pObj->GetGroup() == iGroup)
    {
      if (pPrev)
        pPrev->SetNext(pNext);
      else
        g_pGfxObj = pNext;
      
      delete pObj;
    }
    else
      pPrev = pObj;
      
    pObj = pNext;   
	}

	/* Tell OnDraw to draw the next time it's called */
	g_bRefresh = 1;
  pthread_mutex_unlock(&g_Mutex);  
	return 0;
}

int gfx_setcurrentgroup(int iGroup)
{
  pthread_mutex_lock(&g_Mutex);  
  g_iCurGroup = iGroup;
  pthread_mutex_unlock(&g_Mutex);
  return 0;
}

int gfx_enable_screen_refresh(int bEnable)
{
	g_bEnabled = bEnable;
	return 0;
}
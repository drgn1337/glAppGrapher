/***************************************************************************
                          mem.h  -  description
                             -------------------

  Prototypes for basic memory functions

    begin                : Sun Sep  1 15:54:34 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __MEM_H__
#define __MEM_H__

void* mem_allocate(int nBytes);
void* mem_allocateandcopy(int nBytes, void* pData);
int mem_free(void* pData);

#endif

/***************************************************************************
                          mem.cpp  -  description

  Basic memory functions used for easier multiplatform implementation.

                             -------------------
    begin                : Sun Sep  1 15:54:34 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#endif

#include <stdlib.h>
#include <memory.h>
#include "mem.h"

void* mem_allocate(int nBytes)
{
#ifdef WIN32
	return GlobalAlloc(GPTR, nBytes);
#endif
#ifdef unix
  return calloc(nBytes, 1);
#endif
	return 0;
}

void* mem_allocateandcopy(int nBytes, void* pData)
{
	void* pNew = mem_allocate(nBytes);
	if (!pNew) return 0;
	memcpy(pNew, pData, nBytes);
	return pNew;
}

int mem_free(void* pData)
{
	if (!pData)
		return -1;
#ifdef WIN32
	GlobalFree((HGLOBAL)pData);
	return 0;
#endif
#ifdef __GNUC__
  free(pData);
  return 0;
#endif
	return -1;
}

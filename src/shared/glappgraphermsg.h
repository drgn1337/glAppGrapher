/***************************************************************************
                          glappgraphermsg.h  -  description
                             -------------------
    begin                : Sun Sep  1 17:01:23 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __GLAPPGRAPHERMSG_H__
#define __GLAPPGRAPHERMSG_H__

/* Default hosting port */
#define GLAPPGRAPHERMSG_DEFAULT_PORT	17702

/* Enumerated drawing object types */
#define GLAG_FIRST_OUTSIDE_GLENUM_OBJECT	0x1000
#define GLAG_SPHERES	GLAG_FIRST_OUTSIDE_GLENUM_OBJECT + 0x0001
#define GLAG_VECTORS	GLAG_FIRST_OUTSIDE_GLENUM_OBJECT + 0x0002
#define GLAG_PLANES		GLAG_FIRST_OUTSIDE_GLENUM_OBJECT + 0x0003

/* Command flags (See GLAG_MSG_FUNC) */

/* GLAG_CMD_IMMEDIATE means perform the function once, and perform it now */
#define GLAG_CMD_IMMEDIATE		0x00000001


/* glAppGrapher messages and parameters. The GL means that message parameters
could be OpenGL specific, like GL_POLYGON for example. */

/* GLAG_MSG_CONSOLE: Console message. Message = text. */
#define GLAG_MSG_CONSOLE		0x00000001

/* GLAG_MSG_CLEAR: Clears the render window. */
#define GLAG_MSG_CLEAR			0x00000002

/* GLAG_MSG_LOAD: Loads a previous render. TODO: Implementation */
#define GLAG_MSG_LOAD			0x00000003

/* GLAG_MSG_SAVE: Save the render. TODO: Implementation */
#define GLAG_MSG_SAVE			0x00000004

/* GLAG_MSG_AXISMODE: Draws an XYZ axis in the corner of the screen to
represent the world XYZ axis. TODO: Implementation */
#define GLAG_MSG_AXISMODE		0x00000005

/* GLAG_MSG_FUNC: Calls a function directly. Message: The
zero-terminated name of the function, command flags, and an array of
parameter sizes and parameters:


  1. Zero-Terminated name of the function (i.e. "glEnable")
  2. Command flags (4 bytes)
  3. Parameter count (4 bytes)
  4. Parameter list
		4a. Parameter size in bytes (also refer to "Command parameter size flags")
		4b. Parameter data (if the parameter is a pointer to data, all
		that data goes here)
 
 */
#define GLAG_MSG_FUNC			0x00000006

/* GLAG_MSG_SHUTDOWN: Shuts down the glappgrapher. Message: The
password, if any, to do this. This can be text or binary data. */
#define GLAG_MSG_SHUTDOWN		0x00000007

/* GLAG_MSG_SETGROUPID: Sets the ID of the current group. Default is
0. */
#define GLAG_MSG_SETGROUPID	0x00000008

/* GLAG_MSG_CLEARGROUP: Erases all the entries in the list with a
specific group ID. 0 is ignored. */
#define GLAG_MSG_CLEARGROUP	0x00000009

/* GLAG_MSG_FREEZE: A non-zero value causes all messages to be queued
until this message with a zero value is passed. Then, all queued
messages will be processed in the order of which they were received */
#define GLAG_MSG_FREEZE     0x0000000A

/* GLAG_MSG_FLUSH: Used to execute all queued messages, and then empty
the queue. Here is some example code on how to use the two together in
the test client for the effect of "rendering" a bunch of objects without
actually drawing them until the end:

glag_freeze(1);
glag_draw_primitives(...);
...
glag_freeze(0);
glag_flush();
*/
#define GLAG_MSG_FLUSH      0x0000000B

/* GLAG_MSG_SYNC: The client wants to wait for the glAppGrapher
to finish processing messages or drawing, and then the server
will respond with a one byte packet to signify it's "ready" for
more input. This message has no parameters. */
#define GLAG_MSG_SYNC       0x0000000C

/* GLAG_MSG_IGNORE: A non-zero value causes all future messages,
except for another GLAG_MSG_IGNORE message, to be ignored. This
will happen until GLAG_MSG_IGNORE is passed with a zero value */
#define GLAG_MSG_IGNORE     0x0000000D

/* Command parameter size flags */

/* If the parameter you pass in is a pointer to data, use this
flag when specifying the parameter size */
#define GLAG_IS_POINTER			0x80000000

#endif

/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Sun Sep  1 12:13:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <conio.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "glappgrapherclient.h"
#include <GL/gl.h>	/* Header File For The OpenGL32 Library */

void drawlines()
{
	for (int i=0; i < 20; i++)
	{
		float x = 50 - (float)(rand() % 100);
		float z = 50 - (float)(rand() % 100);
		float fLines[2*3] = { 
			x,0,z,
			x,5 + (float)(rand() % 100),z
		};

		float fClr[8] = {
			(float)(rand() % 100) / 100.0f,
			(float)(rand() % 100) / 100.0f,
			(float)(rand() % 100) / 100.0f,
			0,
			(float)(rand() % 100) / 100.0f,
			(float)(rand() % 100) / 100.0f,
			(float)(rand() % 100) / 100.0f,
			0
		};

		glag_draw_primitives(GL_LINES, fLines, fClr, 0, 2, 0);
	}
}

void drawplanes()
{
	float fPlane[4];
	glag_disable(GL_DEPTH_TEST);
	glag_enable(GL_BLEND);
	glag_blendfunc(GL_SRC_ALPHA, GL_ONE);

	fPlane[0] = 0; fPlane[1] = 1; fPlane[2] = 0; fPlane[3] = 0;
	glag_set_color((float)(rand() % 100) / 100.0f,
		(float)(rand() % 100) / 100.0f,
		(float)(rand() % 100) / 100.0f,
		0.5f);
	glag_draw_primitives(GLAG_PLANES, fPlane, 0, 0, 1, 0);

	fPlane[0] = 0; fPlane[1] = 1; fPlane[2] = 0; fPlane[3] = -200;
	glag_set_color((float)(rand() % 100) / 100.0f,
		(float)(rand() % 100) / 100.0f,
		(float)(rand() % 100) / 100.0f,
		0.5f);
	glag_draw_primitives(GLAG_PLANES, fPlane, 0, 0, 1, 0);

	fPlane[0] = 1; fPlane[1] = 0; fPlane[2] = 0; fPlane[3] = -50;
	glag_set_color((float)(rand() % 100) / 100.0f,
		(float)(rand() % 100) / 100.0f,
		(float)(rand() % 100) / 100.0f,
		0.5f);
	glag_draw_primitives(GLAG_PLANES, fPlane, 0, 0, 1, 0);

	glag_enable(GL_DEPTH_TEST);
	glag_disable(GL_BLEND);
}

int drawspheres()
{
	for (int i=0; i < 10; i++)
	{
		float fSphere[4] = { 50 - (float)(rand() % 100), 50 - (float)(rand() % 100), 50 - (float)(rand() % 100), (float)(5 + rand() % 30) };

		glag_enable(GL_LIGHTING);
		glag_enable(GL_LIGHT0);
		glag_enable(GL_BLEND);
		glag_draw_primitives(GLAG_SPHERES, fSphere, 0, 0, 1, 0);
		glag_disable(GL_LIGHT0);
		glag_disable(GL_LIGHTING);
		glag_disable(GL_BLEND);
	}
	return 0;
}

void demo()
{
	glag_clear();
	printf("Here we see an empty world (press enter)\r\n");
	getchar();
	drawlines();
	printf("Here we see a bunch of lines (press enter)\r\n");
	getchar();
	glag_clear();
	printf("We got rid of the lines (press enter)\r\n");
	getchar();
	drawplanes();
	printf("Now we see planes (press enter)\r\n");
	getchar();
	glag_clear();
	drawspheres();
	printf("Spheres, anyone? (press enter)\r\n");
	getchar();
	glag_clear();
	printf("Goodbye! (press enter)\r\n");
	getchar();
}

int main(int argc, char *argv[])
{
	if (glappgrapherserverconnect("127.0.0.1"))
	{
		printf("Failed to connect to glAppGrapher server");
		return EXIT_FAILURE;
	}

	/* Run a small demo */
	demo();
	
	glappgrapherserverdisconnect();
	return EXIT_SUCCESS;
}

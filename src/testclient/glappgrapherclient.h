/***************************************************************************
                          glappgrapherclient.h  -  description
                             -------------------
    begin                : Sun Sep  1 12:13:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __GLAPPGRAPHERCLIENT_H__
#define __GLAPPGRAPHERCLIENT_H__

#include "../shared/glappgraphermsg.h"

/* Macros that make the job easier */
#define P(type,param) sizeof(type),(type*)&param
#define V(size,param) size|GLAG_IS_POINTER,param

/* Connect to the glAppGrapher server */
int glappgrapherserverconnect(const char* szIP);
/* Disconnect from the glAppGrapher server */
int glappgrapherserverdisconnect();
/* Send a message to the glAppGrapher server */
int glappgraphersendmsg(int msg /* GLAG_MSG_CONSOLE, GLAG_MSG_GL_FUNC, ... */, void* pData, int size);

/* Drawing functions */
int glag_draw_primitives(int type, float* pVertices, float* pColors, unsigned short* pIndices, int nVertices, int nIndices);
int glag_set_color(float r, float g, float b, float a);
int glag_enable(int value);
int glag_disable(int value);
int glag_enableclientstate(int value);
int glag_disableclientstate(int value);
int glag_clear();
int glag_line(float x1, float y1, float z1, float x2, float y2, float z2);
int glag_blendfunc(int iSrc, int iDst);
int glag_print(float x, float y, float z, const char *fmt, ...);
int glag_gfx_cmd(const char* szFuncName, int flags, int nParams,  ...);
int glag_set_group(int iGroupID);
int glag_clear_group(int iGroupID);
int glag_freeze(int iFreeze /* 0 or 1 */);
int glag_flush();
int glag_sync();
int glag_ignore(int iIgnore /* 0 or 1 */);

#endif

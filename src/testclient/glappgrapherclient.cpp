/***************************************************************************
                          glappgrapherclient.cpp  -  description
                             -------------------
    begin                : Sun Sep  1 12:13:11 EDT 2002
    copyright            : (C) 2002 by Chris Haag
    email                : dragonslayer_developer@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include <winsock.h>
#endif
#ifdef unix
#include <arpa/inet.h>
#include <sys/socket.h>
#include <memory.h>
#include <stdarg.h>
#define closesocket(s) shutdown(s, 2)
#endif

#include <GL/gl.h>	/* Header File For The OpenGL32 Library. We need this for
                      enumerations. */
#include <string.h>
#include <stdio.h>
#include "glappgrapherclient.h"
#include "../shared/glappgraphermsg.h"
#include "../shared/mem.h"

#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif
#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

int g_hSocket = INVALID_SOCKET;

int glappgrapherserverconnect(const char* szIP)
{
	struct sockaddr_in sin;

	/* Initialize sockets */
#ifdef WIN32
	WSADATA wsad;
    if(WSAStartup(0x0101,&wsad))
    {
		return -1;
	}
#endif

	g_hSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_IP);
	if (g_hSocket == INVALID_SOCKET)
		return -1;

	sin.sin_family=PF_INET;
	sin.sin_port=htons(GLAPPGRAPHERMSG_DEFAULT_PORT);
	sin.sin_addr.s_addr=inet_addr(szIP);
	if (SOCKET_ERROR == connect(g_hSocket,(struct sockaddr*)&sin,sizeof(struct sockaddr)))
		return -1;

	return 0;
}

int glappgrapherserverdisconnect()
{
	if (g_hSocket == INVALID_SOCKET)
		return -1;
	closesocket(g_hSocket);
	g_hSocket = INVALID_SOCKET;
	return 0;
}

int glappgraphersendmsg(int msg, void* pData, int size)
{
	if (g_hSocket == INVALID_SOCKET)
		return -1;

	/* Send the message type */
	if (SOCKET_ERROR == send(g_hSocket, (const char*)&msg, 4, 0))
		return -1;

	/* Send the message size */
	if (SOCKET_ERROR == send(g_hSocket, (const char*)&size, 4, 0))
		return -1;

	/* Send the message */
	if (pData && size)
	{
		if (SOCKET_ERROR == send(g_hSocket, (const char*)pData, size, 0))
			return -1;
	}
	
	return 0;
}

/* Drawing functions */

int glag_draw_primitives(int type, float* pVertices, float* pColors, unsigned short* pIndices, int nVertices, int nIndices)
{
	if (!pVertices) return -1;
	if (nIndices && !pIndices) return -1;

	switch (type)
	{
	case GLAG_SPHERES:
		return glag_gfx_cmd("GLAG_DrawSpheres", 0, 2,
			sizeof(int), &nVertices,
			nVertices * 4 * sizeof(float), pVertices);
	case GLAG_PLANES:
		return glag_gfx_cmd("GLAG_DrawPlanes", 0, 2,
			sizeof(int), &nVertices,
			nVertices * 4 * sizeof(float), pVertices);
	default:
		{
/*#ifdef _DEBUG
			char sz[512];
			OutputDebugString("glag_draw_primitives:\n");
			sprintf(sz, "Vertices: %d\n", nVertices);
			OutputDebugString(sz);
			for (int i=0; i < nVertices; i++)
			{
				sprintf(sz, "%d: x=%.02f y=%.02f z=%.02f\n", i, pVertices[i*3],
					pVertices[i*3+1], pVertices[i*3+2]);
				OutputDebugString(sz);
			}
			sprintf(sz, "Indices: %d\n", nIndices);
			OutputDebugString(sz);
			for (i=0; i < nIndices; i++)
			{
				sprintf(sz, "%d: %d\n", i, pIndices[i]);
				OutputDebugString(sz);
			}
#endif*/

			GLint size = 3;
			GLenum vtype = GL_FLOAT;
			GLsizei stride = sizeof(float)*3;

			if (glag_gfx_cmd("glVertexPointer", 0, 4,
				P(GLint, size),
				P(GLenum, vtype),
				P(GLsizei, stride),
				V(nVertices * 3 * sizeof(float), pVertices)))
				return -1;

			if (pColors)
			{
				glag_enableclientstate(GL_COLOR_ARRAY);
				size = 4;
				stride = sizeof(float)*4;
				if (glag_gfx_cmd("glColorPointer", 0, 4,
					P(GLint, size),
					P(GLenum, vtype),
					P(GLsizei, stride),
					V(nVertices * 4 * sizeof(float), pColors)))
					return -1;
			}

			if (nIndices)
			{
				GLenum itype = GL_UNSIGNED_SHORT;
				return glag_gfx_cmd("glDrawElements", 0, 4,
					sizeof(GLenum), &type,
					sizeof(GLsizei), &nIndices,
					sizeof(GLenum), &itype,
					V(nIndices * sizeof(unsigned short), pIndices));
			}
			GLint first = 0;

			if (glag_gfx_cmd("glDrawArrays", 0, 3,
				P(GLenum, type),
				P(GLint, first),
				P(GLsizei, nVertices)))
				return -1;

			return glag_disableclientstate(GL_COLOR_ARRAY);
		}
	}
}

int glag_set_color(float r, float g, float b, float a)
{
	float f[4] = { r,g,b,a };
	/* Here's an example of passing in a pointer */
	return glag_gfx_cmd("glColor4fv", 0, 1, V(sizeof(f), f));
}

int glag_enable(int value)
{
	return glag_gfx_cmd("glEnable", 0, 1, P(GLenum, value));
}

int glag_disable(int value)
{
	return glag_gfx_cmd("glDisable", 0, 1, P(GLenum, value));
}

int glag_enableclientstate(int value)
{
	return glag_gfx_cmd("glEnableClientState", 0, 1, P(GLenum, value));
}

int glag_disableclientstate(int value)
{
	return glag_gfx_cmd("glDisableClientState", 0, 1, P(GLenum, value));
}

int glag_blendfunc(int iSrc, int iDst)
{
	int i[2] = { iSrc, iDst };
	return glag_gfx_cmd("glBlendFunc", 0, 2, P(GLenum, i[0]), P(GLenum, i[1]));
}

int glag_print(float x, float y, float z, const char *fmt, ...)
{
	char		text[513] = {0};
	va_list		list;

	if (!fmt) return -1;
	va_start(list, fmt);
	_vsnprintf_s(text, 512, 512, fmt, list);
	va_end(list);

	if (glag_gfx_cmd("glRasterPos3f", 0, 3, P(GLfloat,x), P(GLfloat,y), P(GLfloat,z)))
		return -1;
	return glag_gfx_cmd("GLAG_Print", 0, 1, V(strlen(text)+1, text));
}

/* Command functions */

/* Commands are mapped as:

Function name   (n bytes)
Zero-Terminator (1 byte)
Flags			(4 bytes)
Parameter count (4 bytes)

  Size of first parameter (4 bytes)
  First parameter (p bytes)
  Size of second parameter (4 bytes)
  Second parameter (q bytes)
  ...
  Size of last parameter (4 bytes)
  Last parameter (r bytes)


  If the parameter on the other side is a pointer, the parameter
  size must be the number of bytes the data points to, and the
  parameter must be the data.
*/

int glag_gfx_cmd(const char* szFuncName, int flags, int nParams, ...)
{
	int msgsize;
	va_list marker;
	void* pMsg;
	char* pCurMsg;
	int i;

	/* Calculate message size */

	/* Start with the function name and null terminator */
	msgsize = strlen(szFuncName) + 1;

	/* Continue with flags */
	msgsize += sizeof(int);

	/* Follow with number of parameters */
	msgsize += sizeof(int);

	/* Follow with parameters */
	va_start(marker, nParams);
	for (i=0; i < nParams; i++)
	{
		int param_size_in_bytes = va_arg(marker, int);
		msgsize += sizeof(int);
		msgsize += param_size_in_bytes & ~GLAG_IS_POINTER;
		/* Ignore the data for now */
		va_arg(marker, int);
	}
	va_end(marker);

	/* Build the message */

	/* Add the function name */
	pMsg = pCurMsg = (char*)mem_allocate(msgsize);
	strcpy_s(pCurMsg, msgsize, szFuncName);
	pCurMsg += strlen(szFuncName);
	*pCurMsg++ = 0; /* Null string terminator */

	/* Add the flags */
	*( (int*)pCurMsg ) = flags;
	pCurMsg += 4;

	/* Add the parameter count */	
	*( (int*)pCurMsg ) = nParams;
	pCurMsg += 4;

	/* Add the parameters */
	va_start(marker, nParams);
	for (i=0; i < nParams; i++)
	{
		int param_size_in_bytes = va_arg(marker, int);
		*( (int*)pCurMsg ) = param_size_in_bytes;
		pCurMsg += sizeof(int);
		memcpy(pCurMsg, (void*)va_arg(marker, int), param_size_in_bytes & ~GLAG_IS_POINTER);
		pCurMsg += param_size_in_bytes & ~GLAG_IS_POINTER;
	}
	va_end(marker);

	glappgraphersendmsg(GLAG_MSG_FUNC, pMsg, msgsize);
	mem_free(pMsg);
	return 0;
}

int glag_clear()
{
	return glappgraphersendmsg(GLAG_MSG_CLEAR, 0, 0);
}

int glag_line(float x1, float y1, float z1, float x2, float y2, float z2)
{
	float fVerts[6] = { x1,y1,z1, x2,y2,z2 };
	return glag_draw_primitives(GL_LINES, fVerts, 0, 0, 2, 0);
}

int glag_set_group(int iGroupID)
{
	glappgraphersendmsg(GLAG_MSG_SETGROUPID, &iGroupID, sizeof(int));
	return 0;
}

int glag_clear_group(int iGroupID)
{
	glappgraphersendmsg(GLAG_MSG_CLEARGROUP, &iGroupID, sizeof(int));
	return 0;
}

int glag_freeze(int iFreeze /* 0 or 1 */)
{
	glappgraphersendmsg(GLAG_MSG_FREEZE, &iFreeze, sizeof(int));
	return 0;
}

int glag_flush()
{
	glappgraphersendmsg(GLAG_MSG_FLUSH, 0, 0);
	return 0;
}

int glag_sync()
{
	char szAck[1];
	if (g_hSocket == INVALID_SOCKET)
		return -1;
	glappgraphersendmsg(GLAG_MSG_SYNC, 0, 0);
	if (SOCKET_ERROR == recv(g_hSocket, szAck, 1, 0)) /* Wait for one byte */
		return -1;
	return 0;
}

int glag_ignore(int iIgnore /* 0 or 1 */)
{
	glappgraphersendmsg(GLAG_MSG_IGNORE, &iIgnore, sizeof(int));
	return 0;
}